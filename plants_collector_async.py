import asyncio
import aiohttp
import json

class PlantsCollectorAsync:
    def __init__(self, plots):
        self.plots = plots
        self.URL = 'https://backend-farm.plantvsundead.com/#/farms/'
        self.URL_PARAMS = '?limit=20&offset=0'
        self.headers = {'authorization': 'auth token'}
        self.plants_with_data = dict()

        self.loop = asyncio.get_event_loop()

    def collect_plants_data(self):
        self.loop.run_until_complete(self.gather_plants_data())

    async def gather_plants_data(self):
        sem = asyncio.Semaphore(1)
        timeout = aiohttp.ClientTimeout(total=4)
        async with aiohttp.ClientSession(headers=self.headers, timeout=timeout) as session:
            tasks = []
            for plot in self.plots:
                task = asyncio.create_task(self.get_plant_data_with_sem(sem, session, plot))
                tasks.append(task)

            r = await asyncio.gather(*tasks)
        return r

    async def get_plant_data_with_sem(self, sem, session, plant_id):
        async with sem:
            return await self.get_plant_data(session, plant_id)

    async def get_plant_data(self, session, plant_id):
        plant_data_json = None
        url = self.URL + plant_id
        while not plant_data_json:
            try:
                async with session.get(url) as response:
                    plant_data_json = await response.json()
                    if plant_data_json['status'] != 0:
                        await asyncio.sleep(2)
                        plant_data_json = None

                        for plant in plant_data_json['data']:
                            for tool in plant['activeTools']:
                                if tool['type'] == 'WATER' and tool['count'] < 25:
                                    print(f'https://marketplace.plantvsundead.com/#/farm/{plant["_id"]} has low water level: {tool["count"]}')

                            if 'hasCrow' in plant and plant['hasCrow']:
                                print(f'https://marketplace.plantvsundead.com/#/farm/{plant["_id"]} has crow')

                    else:
                        self.plants_with_data[plant_data_json['data'][0]['ownerId']] = [plant_data_json['data']]

            except Exception:
                return {}

        return True

    def update_plots_data(self):
        with open('plots_data.json', 'w') as f:
            json.dump(self.plants_with_data, f)
        print(self.plants_with_data)

    def update_spawning_time(self, plots_data_filename, sp_time_filename):
        with open(plots_data_filename, 'r') as f:
            plots_data = json.load(f)

        plants_created_at = dict()
        for plot, plants in plots_data.items():
            pages_total = 0
            for plant_page in plants:
                pages_total += 2
                for plant in plant_page:
                    if plant_page.index(plant) > 9:
                        page_number = pages_total
                    else:
                        page_number = pages_total - 1
                    tools = plant['activeTools']
                    for t in tools:
                        if t['type'] == 'WATER':
                            res_time = t['endTime']
                    plants_created_at.update({plant['_id']: [res_time, plant['ownerId'], plant['plantId'], page_number, plant['_id']]})
        print(plants_created_at)
        print('Total plants:', len(plants_created_at))

        with open(sp_time_filename, 'w') as f:
            json.dump(plants_created_at, f)
