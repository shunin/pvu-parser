import requests
import time
import sys
import json


class PlotsCollector:
    def __init__(self):
        self.URL = 'https://backend-farm.plantvsundead.com/land/'
        token = '...'
        self.headers = {'authorization': token}

    def update_addresses(self):
        addresses = list()
        for row in range(-16, 17):
            for col in range(-16, 17):
                time.sleep(1)
                owner_id = self.get_owner_id(str(col), str(row))
                if owner_id:
                    addresses.append(owner_id)

                if len(addresses) % 20 == 0:
                    print(f'Got {len(addresses)} addresses')

        for addr in addresses:
            addr += '\n'
        with open('addresses.txt', 'w') as f:
            f.writelines(addresses)

    def get_owner_id(self, x, y):
        url = self.URL + x + '/' + y
        data = None
        while not data:
            try:
                data = requests.get(url, headers=self.headers, timeout=2).json()
                if data['status'] != 0:
                    time.sleep(2)
                    data = None
            except Exception:
                continue

        owner_id = data['data']['ownerId']
        print(owner_id)

        return owner_id