import requests
import time
import json
import random

from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem


class PlotsParser:
    def __init__(self, plots):
        self.plots = plots
        self.URL = 'https://backend-farm.plantvsundead.com/farms/'
        self.URL_PARAMS = '?limit=20&offset=0'
        self.token_farmila = 'your token'

        # changing user agent
        self.software_names = [SoftwareName.CHROME.value]
        self.operating_systems = [
            OperatingSystem.WINDOWS.value,
            OperatingSystem.LINUX.value,
        ]

        self.user_agent_rotator = UserAgent(
            software_names=self.software_names,
            operating_systems=self.operating_systems,
            limit=100,
        )

        self.plants_with_data = dict()

    def find_unwatered(self):
        tokens = [
            # your tokens list
        ]
        for id_plot in self.plots:
            print('Checking id', id_plot)
            url = self.URL + id_plot + self.URL_PARAMS
            data = None

            while not data:
                try:
                    token_index = random.randint(0, len(tokens) - 1)
                    headers = {'authorization': tokens[token_index],
                               'user-agent': self.user_agent_rotator.get_random_user_agent()}
                    data = requests.get(url, headers=headers).json()

                    if data['status'] != 0:
                        print('Amount collector error, data:', data, token_index)
                        time.sleep(2)
                        data = None

                except Exception as e:
                    print(str(e))
                    time.sleep(1000)
            try:
                plants_total = int(data['total'])

            except:
                print(data)
                continue

            print('Total plants:', plants_total)
            offset = 0
            plants_data = []

            while offset < plants_total:
                #time.sleep(0.05)
                data = None
                url = self.URL + id_plot + '?limit=20&offset=' + str(offset)
                token_index = random.randint(0, len(tokens) - 1)
                headers = {'authorization': tokens[token_index],
                           'user-agent': self.user_agent_rotator.get_random_user_agent()}
                while not data:
                    try:
                        data = requests.get(url, headers=headers).json()
                        if data['status'] != 0:
                            print('Plants collector error, data:', data, token_index)
                            time.sleep(2)
                            token_index = random.randint(0, len(tokens) - 1)
                            headers = {'authorization': tokens[token_index],
                                       'user-agent': self.user_agent_rotator.get_random_user_agent()}
                            data = None

                    except Exception as e:
                        print(str(e))
                        print('data:', data)
                        time.sleep(1000)
                        continue

                plants_data.extend(data['data'])
                offset += 20

                print(f'Checked {offset}')
                print(data)

                if data['data'][0]['ownerId'] in self.plants_with_data:
                    self.plants_with_data[data['data'][0]['ownerId']].append(data['data'])
                else:
                    self.plants_with_data[data['data'][0]['ownerId']] = [data['data']]

            self.update_plants_data()

    def update_plants_data(self):
        with open('plots_data_neww.json', 'r') as f:
            existing_data = json.load(f)

        new_data = existing_data | self.plants_with_data

        with open('plots_data_neww.json', 'w') as f:
            json.dump(new_data, f)

        print('Plots checked:', len(new_data))

    def check_whitelist(self):
        url = 'https://backend-farm.plantvsundead.com/farming-stats'

        tokens = {your_token: 'token name'}

        for token, token_name in tokens.items():
            headers = {'authorization': token}
            status = requests.get(url, headers=headers).json()['status']
            if status == 444:
                msg = '-'
            else:
                msg = '+'

            print(token_name, msg)

