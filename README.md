# PvU parser

- Parser for older version of p2e crypto game Plant vs Undead. 
- I used it like a toolbox for my personal needs so in main file there are just commented lines with some useful methods.
- While coding this I finally managed to use asynchronous requests for speeding up the parsing.
- This project then turned into something bigger that I can't post in public yet because it's up and running.
