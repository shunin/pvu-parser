import aiohttp
import asyncio


class PlotsCollectorAsync:
    def __init__(self):
        self.URL = 'https://backend-farm.plantvsundead.com/land/'
        token = 'your token'
        self.headers = {'authorization': token}
        self.loop = asyncio.get_event_loop()

    def update_addresses(self, filename):
        owner_ids = self.loop.run_until_complete(self.gather_parsing_results())

        ids = ''
        for owner_id in owner_ids:
            ids  += owner_id + '\n'
        with open(filename, 'w') as f:
            f.write(ids)

    async def gather_parsing_results(self):
        tasks = []
        #timeout = aiohttp.ClientTimeout(total=5)
        sem = asyncio.Semaphore(30)
        async with aiohttp.ClientSession(headers=self.headers) as session:
            for row in range(-16, 17):
                for col in range(-16, 17):
                    task = asyncio.create_task(self.get_owner_id_with_sem(sem, session, str(col), str(row)))
                    tasks.append(task)
            results = await asyncio.gather(*tasks)
        return results

    async def get_owner_id_with_sem(self, sem, session, x, y):
        async with sem:
            return await self.get_owner_id(session, x, y)

    async def get_owner_id(self, session, x, y):
        url = self.URL + x + '/' + y
        data = None
        while not data:
            try:
                async with session.get(url) as response:
                    data = await response.json()
                    if data['status'] != 0:
                        await asyncio.sleep(2)
                        data = None
            except Exception:
                continue

        owner_id = data['data']['ownerId']
        print(x, y, owner_id)

        return owner_id