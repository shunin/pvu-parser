import requests
import time
import asyncio
import aiohttp
import json

class PlotsParserAsync:
    def __init__(self, plots):
        self.plots = plots
        self.URL = 'https://backend-farm.plantvsundead.com/#/farms/other/'
        self.URL_PARAMS = '?limit=20&offset=0'
        self.token = 'your token'
        self.headers = {'authorization': self.token}
        self.plots_with_plants_amounts = dict()
        self.plants_with_data = dict()
        self.loop = asyncio.get_event_loop()


    def find_best_plants(self):
        #asyncio.run(self.gather_plots_data())
        self.loop.run_until_complete(self.gather_plots_data())
        #self.loop.run_until_complete(self.gather_best_plants())
        #self.find_best()
        #asyncio.run(self.gather_best_plants())
        self.update_plots_data()
        #print(self.plots_with_plants_amounts)

    async def get_plot_page(self, session, plot):
        url = self.URL + plot + self.URL_PARAMS
        print(f'get url: {url}')
        cntr = 0
        while True:
            try:
                async with session.get(url) as resp:
                    time.sleep(2)
                    plot_data_json = await resp.json()
                    if plot_data_json['status'] == 0:
                        self.plots_with_plants_amounts.update({plot: plot_data_json['total']})
                        return plot_data_json
                    else:
                        print('error1')
                        print(plot_data_json)
                        time.sleep(30)
                        continue
            except Exception as e:
                await asyncio.sleep(1)
                if cntr >= 3:
                    print('shit2')
                    return {}
                cntr += 1
                continue

    async def get_plot_page_with_sem(self, sem, session, plot):
        async with sem:
            return await self.get_plot_page(session, plot)

    async def gather_plots_data(self):
        sem = asyncio.Semaphore(1)
        timeout = aiohttp.ClientTimeout(total=4)
        async with aiohttp.ClientSession(headers=self.headers, timeout=timeout) as session:
            tasks = []
            for plot in self.plots:
                task = asyncio.create_task(self.get_plot_page_with_sem(sem, session, plot))
                tasks.append(task)
            results = await asyncio.gather(*tasks)
        return results

    async def gather_best_plants(self):
        sem = asyncio.Semaphore(1)
        timeout = aiohttp.ClientTimeout(total=4)
        async with aiohttp.ClientSession(headers=self.h, timeout=timeout) as session:
            tasks = []
            for plot in self.plots_with_plants_amounts:
                task = asyncio.create_task(self.get_plants_pages_with_sem(sem, session, plot, self.plots_with_plants_amounts[plot]))
                tasks.append(task)
            r = await asyncio.gather(*tasks)
        return r

    async def get_plants_pages_with_sem(self, sem, session, plot_id, plants_amount):
        async with sem:
            return await self.get_plants_pages(session, plot_id, plants_amount)

    async def get_plants_pages(self, session, plot_id, plants_amount):
        params = '?limit=20&offset='
        offset = 0
        while offset < int(plants_amount):
            cntr = 0
            plant_data_json = None
            url = self.URL + plot_id + params + str(offset)
            while not plant_data_json:
                try:
                    async with session.get(url) as response:
                        plant_data_json = await response.json()
                        if plant_data_json['status'] != 0:
                            await asyncio.sleep(2)
                            plant_data_json = None
                        else:
                            if plant_data_json['data'][0]['ownerId'] in self.plants_with_data:
                                self.plants_with_data[plant_data_json['data'][0]['ownerId']].append(plant_data_json['data'])
                            else:
                                self.plants_with_data[plant_data_json['data'][0]['ownerId']] = [plant_data_json['data']]

                except Exception:
                    await asyncio.sleep(1)
                    if cntr >= 3:
                        return {}
                    cntr += 1
                    continue

            offset += 20
        return True

    def update_plots_data(self):
        with open('plots_data.json', 'w') as f:
            json.dump(self.plants_with_data, f)
        print(self.plants_with_data)

    def update_spawning_time(self, plots_data_filename, sp_time_filename):
        with open(plots_data_filename, 'r') as f:
            plots_data = json.load(f)

        plants_created_at = dict()
        for plot, plants in plots_data.items():
            pages_total = 0
            for plant_page in plants:
                pages_total += 2
                for plant in plant_page:
                    if plant_page.index(plant) > 9:
                        page_number = pages_total
                    else:
                        page_number = pages_total - 1
                    tools = plant['activeTools']
                    for t in tools:
                        if t['type'] == 'WATER':
                            res_time = t['endTime']
                    plants_created_at.update({plant['_id']: [res_time, plant['ownerId'], plant['plantId'], page_number, plant['_id']]})
        print(plants_created_at)
        print('Total plants:', len(plants_created_at))

        with open(sp_time_filename, 'w') as f:
            json.dump(plants_created_at, f)
