from plots_collector_async import PlotsCollectorAsync
from plots_parser import PlotsParser
from plots_parser_async import PlotsParserAsync

import datetime
import json


def update_plots(filename_out):
    pc = PlotsCollectorAsync()
    pc.update_addresses(filename_out)
    print('Plots updated')


def filter_addresses(filename_in, filename_out):
    with open(filename_in, 'r') as f:
        plots = f.readlines()
    plots = [x for x in plots if x.startswith('0x')]  # removing \n
    plots = set(plots)
    s = ''
    for plot in plots:
        s += plot

    with open(filename_out, 'w') as f:
        f.write(s)
    print('Addresses filtered, total available:', len(plots))


def find_plants(filename):
    with open(filename, 'r') as f:
        plots = f.readlines()
        plots = [x[:-1] for x in plots]  # removing \n'''
    plots = list(plots)
    ppa = PlotsParserAsync(plots)
    ppa.find_best_plants()


def update_spawning_time(plots_filename, plots_data_filename, sp_time_filename):
    with open(plots_filename, 'r') as f:
        plots = f.readlines()
        plots = [x[:-1] for x in plots]  # removing \n
    ppa = PlotsParserAsync(plots)
    ppa.update_spawning_time(plots_data_filename, sp_time_filename)


def find_soon_water_reset():
    with open('spawning_time.json', 'r') as f:
        spawning_time = json.load(f)

    messages = []
    current_time = datetime.datetime.now()
    for plant, sp_time in spawning_time.items():
        sp_time_formatted = sp_time[10:-5]
        current_day = current_time.strftime('%Y-%m-%d')
        sp_time_formatted = current_day + sp_time_formatted
        format = '%Y-%m-%dT%H:%M:%S'
        dt = datetime.datetime.strptime(sp_time_formatted, format)

        diff = current_time - dt
        utc_offset = 5
        diff = diff.seconds - 3600*utc_offset

        if 0 < diff < 15:
            messages.append(f'Reset has been {diff} seconds ago: https://marketplace.plantvsundead.com/farm/{plant}')
        elif -60 < diff < 0:
            messages.append(f'Reset will be in {diff*-1} seconds: https://marketplace.plantvsundead.com/farm/{plant}')

    if not messages:
        print('No resets soon')
    else:
        messages.sort()
        if len(messages) == 1:
            print(messages[0])
        else:
            for msg in messages:
                print(msg)


def find_plants_na(filename_in):
    with open(filename_in, 'r') as f:
        plots = f.readlines()
        plots = [x[:-1] for x in plots]  # removing \n'''

    pp = PlotsParser(plots)
    pp.find_unwatered()


def update_coordinates(plants_filename, plants_out_filename):
    with open(plants_filename, 'r') as f:
        plants_data = json.load(f)

    with open('coords.txt', 'r') as f:
        coords = f.readlines()
        coords = [x[:-1].split() for x in coords]

    for plant in plants_data:
        for coord in coords:
            if len(coord) == 3 and plants_data[plant][1] == coord[2]:
                plants_data[plant].append((coord[0], coord[1], plants_data[plant][3]))

    with open(plants_out_filename, 'w') as f:
        json.dump(plants_data, f)

    print(plants_data)


if __name__ == '__main__':
    plots_raw = 'plots_raw.txt'
    plots_filtered = 'plots_filtered.txt'
    plots_data_filename = 'plots_data_neww.json'
    sp_time_filename = 'spawning_time_new_format.json'
    plants_w_coords = 'spawning_time_coords.json'

    ppa = PlotsParser([])
    ppa.check_whitelist()


    #update_plots(plots_raw)
    #filter_addresses(plots_raw, plots_filtered)
    # async find_plants(plots_filtered)
    #find_plants_na(plots_filtered)
    #update_spawning_time(plots_filtered, plots_data_filename, sp_time_filename)
    #update_coordinates(sp_time_filename, plants_w_coords)



